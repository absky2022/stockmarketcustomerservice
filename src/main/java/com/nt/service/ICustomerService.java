package com.nt.service;

import java.util.List;

import com.nt.entity.Customer;

public interface ICustomerService {

	Long saveCustomer(Customer cust);
	Customer getOneCustomer(Long id);
	Customer getOneCustomerByEmail(String email);
	Customer getOneCustomerByPanCard(String panCard);
	Customer getOneCustomerByMobile(String mobile);
	Customer getOneCustomerByAadhar(String aadhar);
	
	List<Customer> getAllCustomers();
}
