package com.nt.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nt.entity.Customer;
import com.nt.exception.CustomerNotFoundException;
import com.nt.repo.CustomerRepository;
import com.nt.service.ICustomerService;

@Service
public class CustomerServiceImpl implements ICustomerService{

	@Autowired
	private CustomerRepository repo;
	
	@Override
	public Long saveCustomer(Customer cust) {
		return repo.save(cust).getId();
	}

	@Override
	public Customer getOneCustomer(Long id) {
		Optional<Customer> opt = repo.findById(id);
		return validateInput(opt,id.toString());
	}

	@Override
	public Customer getOneCustomerByEmail(String email) {
		Optional<Customer> opt = repo.findByEmail(email);
		return validateInput(opt, email);
	}

	@Override
	public Customer getOneCustomerByPanCard(String panCard) {
		Optional<Customer> opt = repo.findByEmail(panCard);
		return validateInput(opt, panCard);
	}

	@Override
	public Customer getOneCustomerByMobile(String mobile) {
		Optional<Customer> opt = repo.findByMobile(mobile);
		return validateInput(opt, mobile);
	}

	@Override
	public Customer getOneCustomerByAadhar(String aadhar) {
		Optional<Customer> opt = repo.findByAadharId(aadhar);
		return validateInput(opt, aadhar);
	}

	@Override
	public List<Customer> getAllCustomers() {
		return repo.findAll();
	}
	
	private Customer validateInput(Optional<Customer> opt, String input) {
		return opt.orElseThrow(
				()-> new CustomerNotFoundException("Customer '"+input+"' Not Found"));
	}

	
}
