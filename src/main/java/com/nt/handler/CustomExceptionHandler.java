package com.nt.handler;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.nt.exception.CustomerNotFoundException;
import com.nt.payload.response.ErrorMessage;

@RestControllerAdvice
public class CustomExceptionHandler {

	@ExceptionHandler(CustomerNotFoundException.class)
	public ResponseEntity<ErrorMessage> handleCompanyNotFoundException(CustomerNotFoundException cnfe){
		
		return ResponseEntity.internalServerError().body(
				new ErrorMessage(
						new Date().toString(),
						cnfe.getMessage(),
						HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR.name()
						)
				);
	}
	
}